The phyloskims docker container
================

<div>

[![](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/docker/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/leca/projects/malbio/phyloskims/-/commits/main)

pipeline status

</div>

The *phyloskims* docker image provides the software programs used to
analyze the genome skims by the
[PhyloAlps](http://phyloalps.org "PhyloAlps web site") and
[PhyloNorway](http://phylonorway.no "PhyloNorway web site") projects.

- The organelle assembler, dedicated to assemble highly repeated
  sequences from genome skim data.

- The organelle annotator, dedicated to produce ready to submit EMBL
  flat files from assembled sequences

\[\[*TOC*\]\]

## Installing the *phyloskim* scripts

Only two small scripts needs to be installed in a repository being part
of your Unix `PATH`. A script is provided to install these scripts. It
can be run using the above command

``` bash
curl -L https://metabarcoding.org/phyloskims/install.sh \
  | bash
```

By default scripts are installed in the /usr/local directory, that
usually require root (administrator) priviledge to be modified. The
install script provides two options to customize the installation, and a
**–hep** option.

- **-i** \| **–install-dir**

  Directory where phyloskims scripts are installed (as example use
  /usr/local not /usr/local/bin).

- **-p** \| **–phyloskims-prefix**

  Prefix added to the obitools command names if you want to have several
  versions of obitools at the same time on your system (as example -p g
  will produce goa command instead of doa).

- **-h** \| **–help**

  Display this help message.

To set these option the command line has to follow the next example.

``` bash
curl -L https://metabarcoding.org/phyloskims/install.sh \
  | bash -s -- --install-dir test_install --phyloskims-prefix k
```

Once installation done, two scripts are available :

- doa : the docker version of the classical [organelle
  assembler](http://metbarcoding.org/asm) command (oa)
- dorgannot : the docker version of the [organnelle
  annotator](http://metabarcoding.org/annot) script

if as in the second example, you used the `--phyloskims-prefix k`
option, script will be named `koa` and `korgannot` to reflect the choice
of the **k** prefix.

These scripts, when you run them, determine if your system is using
`singularity` or `docker` to run docker containers. Both scripts are
providing a –help options.

``` bash
doa --help
```

### Installing docker on your laptop

*Docker* is available for every platform: *Windows*, *MacOs*, *Linux* at
<https://www.docker.com/>. For *MacOS* users, I recommend to use
*OrbStack* (<https://orbstack.dev/>) instead of the genuine *Docker*. It
is fully optimized for *MacOS* and can run blindly *x86* and *M1/M2*
container on *M1/M2* machines. The provided **phyloskims** contenair is
an *x86* container, *MasOS* users, having a *M1* or *M2* computer, will
need to install
[Rosetta](https://support.apple.com/en-us/HT211861#:~:text=You're%20asked%20to%20install,an%20app%20that%20needs%20Rosetta.)
on their machine moreover orbstack.

### Constraints on the usage of the dockerized tools

Both the script `doa` and `dorgannot` run the docker container by
mounting only the current working directory from where the scripts are
run into the docker container. Therefore the dockerized software can
only access to file located into that directory. This impose that every
input or output files passed as arguments to both scripts must resid in
the current directory or in a subfolder of the current directory. It is
not possible to use absolute paths as argument even if they point to a
file correctly located.

## Organelle assembler

The [Organelle Assembler](http://metabarcoding.org/asm) aims to target
the assembling of small sequences over-represented in a whole genome
shotgun sequence dataset. Typically these sequences are

- The organelle genomes
  - The mitochondrion genome
  - The plastid genome for plants
- The nuclear rDNA cluster including: the SSU (18S), ITS1, TSU (5.6S),
  ITS2, and LSU (23S) genes.

The [Organelle Assembler](http://metabarcoding.org/asm) tries do acheive
assembly task in the most possible automatic way. For that it is
learning the optimal parameters for the assembly from the dataset. If it
succeeds, the [Organelle Assembler](http://metabarcoding.org/asm)
provides as output a single contig corresponding to the fully assembled
target sequence. For chloroplastic genome, this include the different
copies of the inverted repeats correctly positionned relatively to each
other. If sequencing depth is too low (below 100x, at worst 50x), or if
many repeats are present in the targeted sequence, or if the difference
of sequencing depth between the organelle genomes (chloroplast /
mitochondrion in plants), the [Organelle
Assembler](http://metabarcoding.org/asm) can fail to assemble the genome
as a single contig. According to the PhyloAlps and PhyloNorway projects,
the [Organelle Assembler](http://metabarcoding.org/asm) succeeds in
assembling the complete chloroplast genome as a single circular contig
for about $2/3$ of the processed genome skims. Most of the failure
belongs three families: *Campanulaceae*, *Cyperaceae*, and *Ericaceae*
because of the numerous repeats present in these genomes.

In case of failing to assemble the targeted sequence as a single contig,
several commands are included into the [Organelle
Assembler](http://metabarcoding.org/asm) to try to fill the gap and
clean up the assembly. At worst it is always possible to dump the
partially assembled sequence in a FASTA file as a set of contigs.

### Example of usage

Hereby, all the instruction needed to assemble the complete chloroplast
genome and the nuclear rDNA cluster of *Dryas octopetala*. The process
is relatively strait forward as the sequencing depth for the chloroplast
is about $70x$. Nevertheless, it illustrates a problematic case caused
by a microsatellite sequence located in the large single copy region
(*LSC*) of the genome.

#### Downloading some data

The genome skim sequence data of *Dryas Octopetala* corresponding to the
voucher *TROM_V\_963983* are downloaded from EBI-SRA database using the
`wget` command.

``` bash
wget http://ftp.sra.ebi.ac.uk/vol1/fastq/ERR555/007/ERR5554787/ERR5554787_1.fastq.gz
wget http://ftp.sra.ebi.ac.uk/vol1/fastq/ERR555/007/ERR5554787/ERR5554787_2.fastq.gz
```

It is also possible to download the same data using the `curl` command.

``` bash
curl http://ftp.sra.ebi.ac.uk/vol1/fastq/ERR555/007/ERR5554787/ERR5554787_1.fastq.gz > ERR5554787_1.fastq.gz
curl http://ftp.sra.ebi.ac.uk/vol1/fastq/ERR555/007/ERR5554787/ERR5554787_2.fastq.gz > ERR5554787_2.fastq.gz
```

#### Indexing the reads

Before being able to assemble some sequences, the organelle assembler
requires for indexing the reads.

``` bash
doa index --estimate-length 0.9 \
    ERR5554787 \
    ERR5554787_1.fastq.gz ERR5554787_2.fastq.gz
```

The organelle assembler algorithm requires that all the indexed reads
must have the same length. Several options of the `index` sub-command
allow for controlling how raw reads are processed before being indexed.
Among them three are controlling the length of the indexed reads.

- **–length** \<INDEX:LENGTH\>

  The length of the read to index (default indexed length is estimated
  from the first read)

- **–estimate-length** \<FRACTION\>

  Estimate the length to index for conserving FRACTION of the overall
  data set

- **–minimum-length** \<INDEX:MINLENGTH\>

  The minimum length of the read to index if the **–estimate-length** is
  activated (default 81)

After indexation the `list` sub command provides information about the
indexed reads.

``` bash
doa list ERR5554787
```

    6618616 97

The command returns two numbers: the count of indexed reads (*e.g.*
\$$6618616$ reads) and the length of indexing (*e.g.* $97$ bp). The
length of indexing must be odd.

#### Assembling the chloroplast genome

The second step consist in identifying the reads belonging the maing
genes of the chloroplast genome using seed sequences. Seed sequences can
be protein sequences (the best) or DNA sequence (for rDNA as example).
These seed sequences can be provided through a fasta file. Some set of
seed sequences are provided with the assembler among them the
`protChloroArabidopsis` include the main protein sequences from the
*Arabidopsis thaliana* chloroplast genome. They allow to initiate
correctly the assembly of every vascular plant chloroplaste genome.

Selection of the reads matching these proteins is done using the `seed`
command of the [Organelle Assembler](http://metabarcoding.org/asm).

In the below command, the `ERR5554787` name corresponds to the read
index previously build from raw data. The `ERR5554787_chloro` name is
the name of the assembling we are initiating. From the `ERR5554787`, it
will be created two assemblies, `ERR5554787_chloro` for assembling the
chloroplast genome, and later on `ERR5554787_rrnanuc` for the assembly
of the nuclear rDNA cluster. The targeted assembled sequence, depends on
the set of seed sequence select by the **–seeds** option of the `seeds`
command.

``` bash
doa seeds --seeds protChloroArabidopsis ERR5554787 ERR5554787_chloro
```

Once the reads are selected, the second step of the assembling process
is the built of the assembly graph. This is acheived by the `buildgraph`
command. At minima the command requires two parameter, the sequence
index (`ERR5554787`) and the newly created assembly
(`ERR5554787_chloro`).

``` bash
doa buildgraph ERR5554787 ERR5554787_chloro
```

On succeed, it is possible to check the assembly graph. The
`ERR5554787_chloro` assembly is actually stored in the
`ERR5554787_chloro.oas` directory. This directory contains an
`assembling.gml` file describing the topology of the assemby graph. This
file follow the [Graph Modelling Language format
(GML)](https://en.wikipedia.org/wiki/Graph_Modelling_Language). It can
be visualized using the Yed program
(<https://www.yworks.com/products/yed>). The next figure shows the
produced assembly graph and legend detailed how to read it. The graph
contains interesting information to further more tude the assembler
parameters if the assembly didn’t complete.

<figure>
<img src="figures/chloro_assembling.svg" style="width:70.0%"
alt="Assembly graph. Each edge corresponds to a fragment of sequence. More green is the edge more it is containing reads matching the probe sequence. The edge labels follow the pattern ID: 5’seq (length) 3’seq [sequencing depth]. IDs can be positive or negative. Two IDs having the same absolute value (e.g. -3/3) corresponds to the same sequence but in reverse/complement orientation. Width of the edges are relative to their sequencing depth." />
<figcaption aria-hidden="true">Assembly graph. Each edge corresponds to
a fragment of sequence. More green is the edge more it is containing
reads matching the probe sequence. The edge labels follow the pattern
ID: 5’seq (length) 3’seq [sequencing depth]. IDs can be positive or
negative. Two IDs having the same absolute value (e.g. -3/3) corresponds
to the same sequence but in reverse/complement orientation. Width of the
edges are relative to their sequencing depth.</figcaption>
</figure>

The last step of the assembly procedure consists in extracting from the
assembly graph the sequence of the contig corresponding to the targeted
sequence. Two commands exist to acheive this task: `unfold` for general
purpose including chloroplast genome and `unfoldrdna` which implement
another euristic to identify the goos path in the assembly graph when
extracting the sequence of the nuclear rDNA cluster.

``` bash
doa unfold ERR5554787 ERR5554787_chloro > ERR5554787_chloro_raw.fasta
```

At the end of the log printed to the standard error output it is
possible to retreive the informations explaining how contigs were chains
to build the complete produced sequence, and the information useful for
checking the validity of the decisions made by the algorithm.

    [INFO ]  Both segments -15 and 25 are connected (paired-end=74 frg length=275.081081 sd=85.148341)
    [INFO ]  Both segments 25 and 13 are connected (paired-end=34 frg length=284.705882 sd=95.096611)
    [INFO ]  Both segments 13 and -14 are disconnected
    [INFO ]     But linked by 24 pair ended links (gap length=4.000000 sd=117.000000)
    [INFO ]  Both segments -14 and -25 are connected (paired-end=58 frg length=300.155172 sd=103.552818)
    [INFO ]  Path is circular and connected by 109  (length: 287, sd: 93)

For that genomes five contigs are scaffolded to produce the chloroplast
genome. The numbers refer to the edge ids on the graph presented above.
Information about the number of pair of reads pass over the junction,
and the insert size corresponding to these paired reads. The contig 13
and -14 are actually disconnected but the algorithm suggest to link them
using a short sequence of 4 Ns to respect the average insert size
observed on the assembly. Later in that tutorial, the reason of that gap
in the asembly will be explored.

These graph topology information are also reported in the header of the
FASTA file produced by the assembler. as a call to the head command on
that file expose it.

``` bash
head ERR5554787_chloro_raw.fasta
```

    >ERR5554787_hq_chloro_1 seq_length=158276; coverage= 72.0; circular=True;  -15 : GAAAA->(18558)->TCTCT  [82] @ {'ERR5554787_hq_chloro_1': [1]}.{connection: 74 - length: 275, sd: 85}.25 : TTAAA->(26318)->TTGGT  [144] @ {'ERR5554787_hq_chloro_1': [2]}.{connection: 34 - length: 284, sd: 95}.13 : TTATA->(6175)->AAAAA  [72] @ {'ERR5554787_hq_chloro_1': [3]}.{N-connection: 24 - Gap length: 4, sd: 117}.-14 : GGATG->(80806)->GAACG  [72] @ {'ERR5554787_hq_chloro_1': [5]}.{connection: 58 - length: 300, sd: 103}.-25 : TGTCA->(26318)->TTAAA  [144] @ {'ERR5554787_hq_chloro_1': [6]}.{connection: 109 - length: 287, sd: 93}
    GAAAAAAAGGATCTCTTCGGGTTTGAAAAACCGATTGTGACTATTCTTTTCGACTATAAA
    AGATGGAATCGTCCGTTGCGGTATATAAAAAACAATCAATTTGAAAATGCTGTAAGAAAT
    GAAATGTCACAATACTTTTTTTATACATGTCAAAGCGATGGAAAAGAAAAAATATCTTTT
    ACGTACCCTCCCAGTTTGGGAACTTTTTTGGAAATGATACAAAGAAAGATGTCTCTGTTC
    ACAAAAGAAAAATTCCCCTCTAATGAGTTTTATAATCATTGGAGTTATACTAATGAACAT
    AAAAAAAAAAACCTAAGCAAAAACTTTATAAATAGAGTAAAAGCTCTCGACAAAACTCTA
    AATGAGGAATCCCTTGTTCTGAATGTACTCGAAAAACGAACTAGATTGTGTAATGATAAG
    ACTAAAAAAGAATACTTACCCCAAATATACGATCCTTTCTTGAATGGACCCTATCGCGTA
    CGAATCAATTTTTTTTTTTCACTCTTAATCATAAATGAAAATTCTATAAAAAATTACATA

##### Exploring the assembly gap

The assembly of the chloroplast genome of Dryas octopetala as done
above, produced a single circular contig, but with a gap of for
nucleotide inserted by the unfold algorithm to get the circularity
between contigs $13$ and $-14$. as recalled in the two following lines
extracted from the unfolding process.

    [INFO ]  Both segments 13 and -14 are disconnected
    [INFO ]     But linked by 24 pair ended links (gap length=4.000000 sd=117.000000)

By looking in the Fasta file using `grep` to analyse that region, it is
possible to locate the missing sequence just at the level of a double
microsatellite `CCCCCCCCCCCCC` then `aaaaaaaaa`.

``` bash
grep -B 3 -A 3 -i nnn ERR5554787_chloro_raw.fasta
```

When the [Organelle Assembler](http://metabarcoding.org/asm) insert such
strech of Ns to fill a gap, A part of the sequence just after the gap is
printed in lower cases, while the remaining of the sequence is
uppercases. These lower cases indicates that in case of short gaps (like
here only 4 Ns), that sequence can be partially redundante with the
sequence before the strech of Ns.

By observing the sequence it is possible to suppose that the poly-C
before the Ns is complete while the poly-A is partial, and the oppisite
for the same partern after the Ns in lowercases.

    CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTTAAATT
    TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT
    TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT
    CGAGCTCCATCGTGTACTATTTACATCACCCCCCCCCCCCCAAAAAAAAAANNNNccccc
    caaaaaaaaagagggttccagtgtaacagaacaaatgatgtcgagccaagagcactttca
    ttcctatataattatatatataaaaaaatggtGGATGTAAGAATCCACAACTGATTGTGT
    CCTTCAAGTCGCACGTTGCTTTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT

If that is true, this means that both the partial poly-C and the partial
poly-A are redundant with their complete versions and can be erased. In
the sequence presented below the deleted nucleotides have been
substituted by dots.

    CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTTAAATT
    TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT
    TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT
    CGAGCTCCATCGTGTACTATTTACATCACCCCCCCCCCCCC...................
    .aaaaaaaaagagggttccagtgtaacagaacaaatgatgtcgagccaagagcactttca
    ttcctatataattatatatataaaaaaatggtGGATGTAAGAATCCACAACTGATTGTGT
    CCTTCAAGTCGCACGTTGCTTTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT

the actual edition is done in a copy of the original file using a
classical text editor.

``` bash
cp ERR5554787_chloro_raw.fasta ERR5554787_chloro_edited.fasta
nano ERR5554787_chloro_edited.fasta
```

Here the `nano` editor was used but any text editor can be used. Once
edited the corresponding part of sequence file look like the below
sequence.

    CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTTAAATT
    TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT
    TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT
    CGAGCTCCATCGTGTACTATTTACATCACCCCCCCCCCCCC
    aaaaaaaaagagggttccagtgtaacagaacaaatgatgtcgagccaagagcactttca
    ttcctatataattatatatataaaaaaatggtGGATGTAAGAATCCACAACTGATTGTGT
    CCTTCAAGTCGCACGTTGCTTTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT

To reformat correctly the FASTA file it is possible to use an OBITools
(<http://metabarcoding.org/obitools4>).

``` bash
obiannotate --length ERR5554787_chloro_edited.fasta > xxx.temp.fasta
mv xxx.temp.fasta ERR5554787_chloro_edited.fasta
```

The `obiannotate`, moreover reformating the FASTA file, recomputes the
actual sequence length of the edited sequence.

``` bash
head ERR5554787_chloro_edited.fasta
```

    >ERR5554787_chloro_1 {"circular":true,"coverage":72,"seq_length":158256} -15 : GAAAA->(18558)->TCTCT  [82] @ {'ERR5554787_hq_chloro_1': [1]}.{connection: 74 - length: 275, sd: 85}.25 : TTAAA->(26318)->TTGGT  [144] @ {'ERR5554787_hq_chloro_1': [2]}.{connection: 34 - length: 284, sd: 95}.13 : TTATA->(6175)->AAAAA  [72] @ {'ERR5554787_hq_chloro_1': [3]}.{N-connection: 24 - Gap length: 4, sd: 117}.-14 : GGATG->(80806)->GAACG  [72] @ {'ERR5554787_hq_chloro_1': [5]}.{connection: 58 - length: 300, sd: 103}.-25 : TGTCA->(26318)->TTAAA  [144] @ {'ERR5554787_hq_chloro_1': [6]}.{connection: 109 - length: 287, sd: 93}
    gaaaaaaaggatctcttcgggtttgaaaaaccgattgtgactattcttttcgactataaa
    agatggaatcgtccgttgcggtatataaaaaacaatcaatttgaaaatgctgtaagaaat
    gaaatgtcacaatactttttttatacatgtcaaagcgatggaaaagaaaaaatatctttt
    acgtaccctcccagtttgggaacttttttggaaatgatacaaagaaagatgtctctgttc
    acaaaagaaaaattcccctctaatgagttttataatcattggagttatactaatgaacat
    aaaaaaaaaaacctaagcaaaaactttataaatagagtaaaagctctcgacaaaactcta
    aatgaggaatcccttgttctgaatgtactcgaaaaacgaactagattgtgtaatgataag
    actaaaaaagaatacttaccccaaatatacgatcctttcttgaatggaccctatcgcgta
    cgaatcaatttttttttttcactcttaatcataaatgaaaattctataaaaaattacata

The previous length of the chloroplast genome was $158276 bp$ and is
after the edition $158256 bp$.

Using *BLAST* at the *NCBI* Website, the edited region of the corrected
sequence can be align with RefSeq entry\* NC_073102.1, an already
published complete chloroplast genome of *Dryas octopetala var.
asiatica* (Length: $158271 bp$).

    Query  1     CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTTAAATT  60
                 |||||||||||||||||||||||||||||||||||||||||||||||||||||| |||||
    Sbjct  5901  CTTTACTTTTGATTTAATCGAAAGAGTTTTACCAATTCCAACAAATTTTACTTTAAAATT  5960

    Query  61    TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT  120
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    Sbjct  5961  TTCTTGAATTGGATCCTTTGGATTTATATATCGAAAATATACTTACGAAGTTGTTCCAAT  6020

    Query  121   TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT  180
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    Sbjct  6021  TTATTGATTGATACTAACCTTAGATTCTTGCCCCTGAGAAATGAATCAATACTTTCTACT  6080

    Query  181   CGAGCTCCATCGTGTACTATTTACATCAcccccccccccccaaaaaaaaaGAGGGTTCCA  240
                 ||||||||||||||||||||||||||||||||||||||   |||||||||||||||||||
    Sbjct  6081  CGAGCTCCATCGTGTACTATTTACATCACCCCCCCCCCAAAAAAAAAAAAGAGGGTTCCA  6140

    Query  241   GTGTAACAGAACAAATGATGTCGAGCCAAGAGCACTTTCATTCCTATATAATTATATATA  300
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    Sbjct  6141  GTGTAACAGAACAAATGATGTCGAGCCAAGAGCACTTTCATTCCTATATAATTATATATA  6200

    Query  301   TaaaaaaaTGGTGGATGTAAGAATCCACAACTGATTGTGTCCTTCAAGTCGCACGTTGCT  360
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    Sbjct  6201  TAAAAAAATGGTGGATGTAAGAATCCACAACTGATTGTGTCCTTCAAGTCGCACGTTGCT  6260

    Query  361   TTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT  400
                 ||||||||||||||||||||||||||||||||||||||||
    Sbjct  6261  TTCTACCACATCGTTTTAAACGAAGTTTTACCATAACATT  6300

The *BLAST* alignment of the edited region with the *RefSeq entry*
[NC_073102.1](https://www.ncbi.nlm.nih.gov/nucleotide/NC_073102.1). The
edited region at position 178 of the query match perfectly with the
already published genome. Only a polymorphism at the level of the
microsatellite is observable. Similar alignments are observable with
other published genomes of *Dryas octopetala* species or of the genus
*Dryas*.

#### Assembling the nuclear rDNA cluster

``` bash
doa seeds --seeds nucrRNAArabidopsis ERR5554787 ERR5554787_rrnanuc
```

``` bash
doa buildgraph ERR5554787 ERR5554787_rrnanuc
```

``` bash
doa unfoldrdna ERR5554787 ERR5554787_rrnanuc > ERR5554787_rrnanuc.raw.fasta
```

``` bash
head ERR5554787_rrnanuc.raw.fasta
```

    >ERR5554787_rrnanuc_1 seq_length=6895; coverage=1971.0; circular=False;  82 : GACGA->(6756)->CAAGC  [1971] @ {'ERR5554787_rrnanuc_1': [1]}.{connection: 138 - length: 249, sd: 109}.7 : ACGAC->(30)->TATCC  [6048] @ {'ERR5554787_rrnanuc_1': [2]}.{connection: 60 - length: 121, sd: 8}.69 : ACACA->(12)->GTGGG  [8841] @ {'ERR5554787_rrnanuc_1': [3]}
    tctaggcatcccaatcaagcaagcaagcaaagcaactccgaagggacaaggggcaagtcc
    acccaaggtacgaggcctcggggcgtttcaaaggaaaGACGAAGGGGGGGAGGGACGAAT
    CGGAGCGACGCAGGGCTGAATCTCAGTGGATCGTGGCAGCAAGGCCACTCTGCCACTTAC
    AATACCCCGTCGCGTATTTAAGTCGTCTGCAAAGGATTCTACCCGCCGCTCGGTGGAAAT
    TGTACTTCAAGGCGGCCCCCGCGGCTCGTCCGCCGCGAGGGCTTCACCAACGACACGTGC
    CTTTGGGGGCCAAGGGCCCCTACTGCAGGTCGGCAATCGGGCGGCGGGCGCATGCGTCGC
    TTCTAGCCCGGATTCTGACTTAGAGGCGTTCAGTCATAATCCAGCGCACGGTAGCTTCGC
    GCCACTGGCTTTTCAACCAAGCGCGATGACCAATTGTGCGAATCAACGGTTCCTCTCGTA
    CTAGGTTGAATTACTATTGCGACACTGTCATCAGTAGGGTAAAACTAACCTGTCTCACGA

### global options

- **-h** \| **–help**

  show this help message and exit

- **–version**

  Print the version of The Organelle Assembler

- **–log** \<LOGFILENAME\>

  Create a logfile

- **–no-progress**

  Do not print the progress bar during analyzes

### sub commands

- **buildgraph** Build the initial assembling graph
- **clone** copy an assembly
- **compact** Recompact the assembling graph
- **cutlow** Cut low coverage edge in an assembling graph
- **graph** Build a graph file from the assembling graph
- **graphstats** Print some statistics about the assembling graph
- **index** Index a set of reads
- **list** List information about a read index
- **restore** Restore the intermediate graph saved regularly during the
  assembling process.
- **seeds** Build the set of seed reads
- **unfold** Universal assembling graph unfolder
- **unfoldrdna** Assembling graph unfolder for the nuclear rDNA complex

## Organelle annotator

The organelle annotator is converting the FASTA file generated by the
assembler to an annotated EMBL flat-file ready to be submitted to EBI.
The dorgannot annotates :

- the inverted repeats using repseek (Achaz et al. 2007)

- the large single copy (LSC) and small single copy (SSC) regions

- the tRNA (using aragorn, ) and differentiate among the three type of
  CAU tRNA (met, fmet and leu)

- the rRNA

``` bash
dorgannot -c \
      -t 57948 -L TV963983 \
      -s TROM_V_963983 -P PRJEB43458 \
      --list-file TROM_V_963983_chloro.chrlist  \
      ERR5554787_chloro.edited.fasta \
      > TROM_V_963983_chloro.embl
```

``` bash
dorgannot -r \
      -t 57948 -L TV963983 \
      -s TROM_V_963983 -P PRJEB43458 \
      --list-file TROM_V_963983_nucrrna.chrlist  \
      ERR5554787_rrnanuc.raw.fasta \
      > TROM_V_963983_nucrrna.embl
```

### Options:

Defining the sequence category

- **-c** \| **–chloroplast**

  Selects for the annotation of a chloroplast genome This is the default
  mode

- **-r** \| **–nuclear-rdna**

  Selects for the annotation of the rDNA nuclear cistron

Providing information about the sequence

- **-s** \| **–specimen** \###

  Represents the specimen voucher identifier (*e.g.* for herbarium
  sample TROM_V\_991090 will be added to the /specimen_voucher qualifier

- **-t** \| **–ncbi-taxid** \###

  Represents the ncbi taxid associated to the sequence

- **-o** \| **–organism** \<organism_name\>

  Allows for specifiying the organism name in the embl generated file
  Spaces have to be substituted by underscore ex : Abies_alba

- **-b** \| **–country** “\<country_value\>\[:\<region\>\]\[,
  \<locality\>\]”

  Location (at least country) of collection

- **-f** \| **–not-force-ncbi**

  Do not force the name of the organism to match the NCBI scientific
  name. if the provided NCB taxid is publically defined

Information related to an ENA project

- -P \| **–project** \<ENA Project\>

  The accession number of the related project in ENA

- -i \| **–id-prefix** \<prefix\>

  prefix used to build the sequence identifier the number of the contig
  is append to the prefix to build the complete id. This id is used only
  if an ENA project is specified.

- -L \| **–locus-prefix** \<prefix\>

  Prefix used to build the locus tag of every annotated genes generated
  locus tags follow the pattern : prefix\_###, where \### is a number
  following the order of gene in the EMBL flat file starting at locus
  tag shift (default 1).

- **-S** \| **–locus-shift** \<###\>

  Start number for building locus tags

- **–list-file** \<FILENAME\>

  The chromosome list file name to file for ENA submission

Annotation of partial sequences

- **-p** \| **–partial**

  Indicates that the genome sequence is partial and therefore in several
  contigs

- **-l** \| **–min-length**

  Indicates for partial mode the minimum length of contig to annotate

Setting up the sequence annotation

- **-N** \| **–no-normalization**

  Does not normalize the sequence before annotation

- **-I** \| **–no-ir-detection**

  Does not look for inverted repeats in the plastid genome

- **-C** \| **–no-cds**

  Do not annotate CDSs

- **-D** \| **–no-cds-pass1**

  Do not annotate core CDSs

- **-E** \| **–no-cds-pass2**

  Do not annotate rps12 CDS

- **-F** \| **–no-cds-pass3**

  Do not annotate shell and dust CDSs

- **-T** \| **–no-trna**

  Do not look for transfer RNA

- **-R** \| **–no-rrna**

  Do not look for ribosomal RNA

## Submission to EBI-ENA

The manifiest file `TROM_V_963983.chloro.manifest` has to be created to
link the assembly to a sample in the EBI database

    STUDY           PRJEB43458
    SAMPLE          SAMEA8202390
    RUN_REF         ERR5554787
    ASSEMBLYNAME    BXA_BVB_1
    ASSEMBLY_TYPE   isolate
    COVERAGE        65
    PROGRAM         Organelle Assembler (https://metabarcoding.org/asm)
    PLATFORM        ILLUMINA
    MINGAPLENGTH    5
    MOLECULETYPE    genomic DNA
    FLATFILE        TROM_V_963983.chloro.embl.gz
    CHROMOSOME_LIST TROM_V_963983.chloro.chrlist.gz

## References

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-Achaz2007-sm" class="csl-entry">

Achaz, Guillaume, Frédéric Boyer, Eduardo P C Rocha, Alain Viari, and
Eric Coissac. 2007. “<span class="nocase">Repseek, a tool to retrieve
approximate repeats from large DNA sequences</span>.” *Bioinformatics*
23 (1): 119–21. <https://doi.org/10.1093/bioinformatics/btl519>.

</div>

</div>
