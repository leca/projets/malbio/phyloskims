#!/bin/bash

# default values
INSTALL_DIR="/usr/local"
PHYLOSKIMS_PREFIX="d"

function download {
    script=$1
    curl "https://gricad-gitlab.univ-grenoble-alpes.fr/leca/projets/malbio/phyloskims/-/raw/main/${script}?inline=false"
}

# help message
function display_help {
  echo "Usage: $0 [OPTIONS]"
  echo ""
  echo "Options:"
  echo "  -i, --install-dir       Directory where phyloskims scripts are installed "
  echo "                          (as example use /usr/local not /usr/local/bin)."
  echo "  -p, --phyloskims-prefix Prefix added to the obitools command names if you"
  echo "                          want to have several versions of obitools at the"
  echo "                          same time on your system (as example -p g will produce "
  echo "                          goa command instead of doa)."
  echo "  -h, --help              Display this help message."
}

while [ "$#" -gt 0 ]; do
  case "$1" in
    -i|--install-dir)
      INSTALL_DIR="$2"
      shift 2
      ;;
    -p|--phyloskims-prefix)
      PHYLOSKIMS_PREFIX="$2"
      shift 2
      ;;
    -h|--help)
      display_help  1>&2 
      exit 0
      ;;
    *)
      echo "Error: Unsupported option $1"  1>&2
      exit 1
      ;;
  esac
done

# the temp directory used, within $DIR
# omit the -p parameter to create a temporal directory in the default location
# WORK_DIR=$(mktemp -d -p "$DIR"  "obitools4.XXXXXX" 2> /dev/null || \
#            mktemp -d -t "$DIR"  "obitools4.XXXXXX")

WORK_DIR=$(mktemp -d "phyloskims.XXXXXX")

# check if tmp dir was created
if [[ ! "$WORK_DIR" || ! -d "$WORK_DIR" ]]; then
  echo "Could not create temp dir"  1>&2
  exit 1
fi

mkdir -p "${INSTALL_DIR}/bin" 2> /dev/null \
  || (echo "Please enter your password for installing phyloskims scripts in ${INSTALL_DIR}"  1>&2
      sudo mkdir -p "${INSTALL_DIR}/bin")

if [[ ! -d "${INSTALL_DIR}/bin" ]]; then
  echo "Could not create ${INSTALL_DIR}/bin directory for installing phyloskims scripts"  1>&2
  exit 1
fi

INSTALL_DIR="$(cd "$INSTALL_DIR" && pwd)"

echo "WORK_DIR=$WORK_DIR"  1>&2
echo "INSTALL_DIR=$INSTALL_DIR"  1>&2
echo "PHYLOSKIMS_PREFIX=$PHYLOSKIMS_PREFIX"  1>&2

pushd "$WORK_DIR"|| exit

download doa > doa
download dorgannot > dorgannot

chmod +x doa dorgannot

doadest=$(sed -E 's/^d/'${PHYLOSKIMS_PREFIX}'/' <<< doa)
dorgannotdest=$(sed -E 's/^d/'${PHYLOSKIMS_PREFIX}'/' <<< dorgannot)

(cp doa "${INSTALL_DIR}/bin/$doadest" 2> /dev/null) \
   || (echo "Please enter your password for installing obitools in ${INSTALL_DIR}" 
       sudo cp doa "${INSTALL_DIR}/bin/$doadest" )

(cp dorgannot "${INSTALL_DIR}/bin/$dorgannotdest" 2> /dev/null) \
   || (echo "Please enter your password for installing obitools in ${INSTALL_DIR}" 
       sudo cp dorgannot "${INSTALL_DIR}/bin/$dorgannotdest" )

popd || exit

rm -rf "$WORK_DIR"

